# suckless_software
---

# My personal builds of DWM, ST, DMENU, and other stuff that is suckless in nature. 
	
NOTE: These builds heavily rely on my dotfiles to work properly. 

	If you want to use my dotfiles and make them your own, feel free to do so

<https://gitlab.com/linuxdabbler/suckless_software>
	
NOTE:  All testing done on Debian 11/testing or Devuan Chimaera/Daedalus.

---
## Instructions for Installation

* Install dependencies

		sudo apt install xorg xserver-xorg libx11-dev libxft-dev libxinerama-dev libxcb1-dev libxinerama-dev gcc make

* git clone this repository

		 git clone https://gitlab.com/linuxdabbler/suckless_software

* cd into each folder and run:

		make

* symlink dwm binary to somewhere in your $PATH
* symlink st binary to somewhere in your $PATH
* symlink dmenu, dmenu_run, dmenu_path, and stest to /usr/local/bin
* copy or symlink dwm.desktop to /usr/share/xsessions
	
NOTE: you can run `sudo make install` to install all of these programs, but
I usually just symlink everything so I just have to run make and the
existing symlinks reflect the new built binary.

---

## DWM-6.4 - suckless window manager with my favorite patches

![empty_desktop_image](images/desktop_empty.jpg)

![busy_desktop_image](images/busy_desktop.jpg)

---
### Patches	
[alwayscenter](https://dwm.suckless.org/patches/alwayscenter/)

[attachbottom](https://dwm.suckless.org/patches/attachbottom/)

[cool-autostart](https://dwm.suckless.org/patches/cool_autostart/)

[fakefullscreen](https://dwm.suckless.org/patches/fakefullscreen/)

[fullgaps-toggle](https://dwm.suckless.org/patches/fullgaps/)

[hide_vacant_tags](https://dwm.suckless.org/patches/hide_vacant_tags)

[pertag](https://dwm.suckless.org/patches/hide_vacant_tags/)

[preserveonrestart](https://dwm.suckless.org/patches/preserveonrestart/)

[status2d-systray](https://dwm.suckless.org/patches/status2d/)

[titlecolor](https://dwm.suckless.org/patches/titlecolor)

[bottomstack](https://dwm.suckless.org/patches/bottomstack)

---

### Features
	* custom bar script (requires NotoColorEmoji)
	* custom keybindings for volume, brightness, and keyboard backlight control (requires scripts from my dotfiles repo or equivalent)
	* custom scripts for scratchpads (requires xdo, and xdotool and scripts from my dotfiles or equivalent)
	* custom logout menu
	* example ~/.xinitrc for restarting without completely logging out.
	* dwm.desktop file to copy to /usr/share/xsessions (for use with display managers like lightdm, ly, gdm, etc...)
---

## ST-0.9 - suckless terminal with my favorite patches

![st_picture](images/st.jpg)

### Patches
[alpha](https://st.suckless.org/patches/alpha/)

[anysize](https://st.suckless.org/patches/anysize/)

[copyurl](https://st.suckless.org/patches/copyurl/)

[delkey](https://st.suckless.org/patches/delkey/)

[font2](https://st.suckless.org/patches/font2/)

[scrollback](https://st.suckless.org/patches/scrollback/)

---

## DMENU-5.2 - my favorite suckless program

![dmenu_picture](images/dmenu.jpg)

### Patches
[alpha](https://tools.suckless.org/dmenu/patches/alpha)

[center](https://tools.suckless.org/dmenu/patches/center)

[lineheight](https://tools.suckless.org/dmenu/patches/line-height)

---

## SLSTATUS - suckless status bar

---

### Features
	* includes bash scripts for battery, brightness, cpu usage, ram usage, updates (apt), disk usage, volume, date, and time.
	* includes color-config.h and colorless-config.h as examples if you want a nice status bar with or without colors.

NOTE:  these scripts require a color emoji font to be installed to work properly.
         
		  sudo apt install fonts-noto-color-emoji
