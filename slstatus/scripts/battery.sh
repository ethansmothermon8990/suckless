#!/bin/bash 

bat() {
	batstat="$(cat /sys/class/power_supply/BAT0/status)"
	batstat1="$(cat /sys/class/power_supply/BAT1/status)"
	bat0="$(cat /sys/class/power_supply/BAT0/capacity)"
	bat1="$(cat /sys/class/power_supply/BAT1/capacity)"

	[ -z $bat0 ] && echo "" || \
	[ -z $bat1 ] && capacity="$bat0" \
	|| capacity="$(( (bat0+bat1) /2))"
    
	if [ $batstat = 'Charging' ] || [ $batstat1 = 'Charging' ]; then
    batstat="🔌"
	echo "$batstat $capacity%"
	elif [[ $capacity -ge 1 ]] && [[ $capacity -le 25 ]]; then
    batstat="🪫"
	echo "$batstat $capacity%"
    elif [[ $capacity -ge 26 ]]; then
    batstat="🔋"
	echo "$batstat $capacity%"
    fi
}
echo $(bat)
