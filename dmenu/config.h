/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */
//# include "colors/gruvbox.h"
static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static const unsigned int alpha = 0xee;     /* Amount of opacity. 0xff is opaque             */
static int centered = 0;                    /* -c option; centers dmenu on screen */
static int min_width = 900;                    /* minimum width when centered */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	"Source Code Pro for Powerline:size=14","NotoColorEmoji:size=14:antialias=true:autohint=true"
};
static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */
static const char *colors[SchemeLast][2] = { 
	/*     fg         bg       */
	[SchemeNorm] = { "#ebdbb2", "#222526" },
	[SchemeSel] = { "#1d2021", "#98971a" },
	[SchemeOut] = { "#ebdbb2", "#8ec07c" },
}; 

static const unsigned int alphas[SchemeLast][2] = {
	[SchemeNorm] = { OPAQUE, alpha },
	[SchemeSel] = { OPAQUE, alpha },
	[SchemeOut] = { OPAQUE, alpha },
};
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 0;
/* -h option; minimum height of a menu line */
static unsigned int lineheight = 32;
static unsigned int min_lineheight = 8;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";
